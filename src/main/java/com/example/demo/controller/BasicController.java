package com.example.demo.controller;


import com.example.demo.model.Movie;
import com.example.demo.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class BasicController {
    @Autowired
    private MovieService movieService;
    
    @RequestMapping("/")
    String hola() {
        return "index";
    }
    @RequestMapping("/add")
    String add(){
        return "addMovie";
    }
    @RequestMapping("/delete")
    String delete(){
        return "delete";
    }
    @RequestMapping("/edit")
    String edit(){
        return "edit";
    }

    @PostMapping("/add")
    String prueba(@RequestParam String movie_name, @RequestParam String url, Model model){
        Movie m= new Movie();
        m.setUrl(url);
        m.setMovie_name(movie_name);
        movieService.addMovie(m);

        model.addAttribute("message", "The movie" + movie_name + "was added");

        return "index";
    }
    @DeleteMapping("/delete")
    String deleteMovie(@RequestParam Long id, Model model) {
        movieService.deleteMovie(id);
        model.addAttribute("message", "The movie with id '" + id + "' was deleted");

        return "index";
    }
    @PutMapping("/edit")
    String editMovie(@RequestParam Long id, @RequestParam String movie_name, @RequestParam String url, Model model) {
        Movie m= new Movie();
        m.setUrl(url);
        m.setMovie_name(movie_name);
        movieService.updateMovie(id,m);
        model.addAttribute("message", "The changes for the movie with id'" + id + "' were saved and updated");

        return "index";
    }
}